﻿using System.Collections.Generic;

namespace Infrastructure.FlatFileReader
{
    public interface IRowModifier
    {
        List<object> Modify(List<object> rowToModify);

        bool Validate(List<object> rowToValidate);
    }
}