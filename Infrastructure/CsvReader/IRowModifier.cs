﻿using System.Collections.Generic;

namespace Alchemy.Infrastructure.CsvReader
{
    public interface IRowModifier
    {
        List<object> Modify(List<object> rowToModify);

        bool Validate(List<object> rowToValidate);
    }
}