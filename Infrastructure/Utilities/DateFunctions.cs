﻿using System;
using System.Globalization;

namespace Infrastructure.Utilities
{
    public static class DateFunctions
    {
        public static string GetYearNoWeekNo(this DateTime date)
        {
            string yearNoWeekNo = string.Empty;

            string yearNumber = date.GetISOYearNumber().ToString();
            string weekNumber = date.GetISOWeekNumber().ToString().PadLeft(2, '0').Right(2);

            yearNoWeekNo = yearNumber + weekNumber;

            return yearNoWeekNo;
        }

        public static int GetISOYearNumber(this DateTime date)
        {
            int weekNumber = date.GetISOWeekNumber();
            int year = date.Year;

            if (weekNumber > 50 && date.Month == 1)
            {
                year = year - 1;
            }
            else if(weekNumber == 1 && date.Month == 12)
            {
                year = year + 1;
            }

            return year;
        }

        public static int GetISOWeekNumber(this DateTime date)
        {
            int daysToAdd = date.DayOfWeek != DayOfWeek.Sunday ? DayOfWeek.Thursday - date.DayOfWeek : (int)DayOfWeek.Thursday - 7;
            date = date.AddDays(daysToAdd);
            var currentCulture = CultureInfo.CurrentCulture;
            return currentCulture.Calendar.GetWeekOfYear(date, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }
    }
}