﻿using Ionic.Zip;
using System.IO;
using System.Linq;
using System.Threading;

namespace Infrastructure.Compression
{
    public class ZipFiles
    {
        public static void ExtractAll(string zipFilePath, string zipFileName)
        {
            string zipFile = Path.Combine(zipFilePath, zipFileName);

            using (ZipFile zip = new ZipFile(zipFile))
            {
                foreach (ZipEntry entry in zip.Entries)
                {
                    entry.Extract(zipFilePath, ExtractExistingFileAction.OverwriteSilently);

                    WaitForFileToExist(zipFilePath, entry.FileName);
                }
            }
        }



        public static void ExtractFile(string zipFilePath, string zipFileName, string fileNameToExtract)
        {
            string zipFile = Path.Combine(zipFilePath, zipFileName);

            using (ZipFile zip = new ZipFile(zipFile))
            {
                ZipEntry fileToExtract = zip.Entries.FirstOrDefault(e => e.FileName.ToLower() == fileNameToExtract.ToLower());

                if(fileToExtract != null)
                {
                    fileToExtract.Extract(zipFilePath, ExtractExistingFileAction.OverwriteSilently);

                    WaitForFileToExist(zipFilePath, fileNameToExtract);
                }
            }
        }

        private static void WaitForFileToExist(string filePath, string fileName)
        {
            int counter = 0;

            do
            {
                if (File.Exists(Path.Combine(filePath, fileName)))
                {
                    FileStream stream = null;
                    try
                    {
                        stream = File.Open(Path.Combine(filePath, fileName), FileMode.Open, FileAccess.ReadWrite, FileShare.None);

                        break;  //the file has finished extracting from zip
                    }
                    catch (IOException)
                    {
                        //the file is unavailable because it is:
                        //still being written to
                        //or being processed by another thread
                        //or does not exist (has already been processed)

                        Thread.Sleep(1000);
                        counter++;
                    }
                    finally
                    {
                        if (stream != null)
                            stream.Close();
                    }
                }

                if (counter > 50)   // if it hasn't finished after 50 seconds something has gone wrong
                    break;

            } while (true);
        }
    }
}
