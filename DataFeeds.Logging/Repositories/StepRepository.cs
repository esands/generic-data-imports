﻿using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace DataFeeds.Logging.Repositories
{
    public class StepRepository
    {
        string _connectionString;

        public StepRepository()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["ProcessLogging"].ConnectionString;
        }

        public Step Start(Step step)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "DL_Process_Step_Start";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@ProcessID", step.process.ID);
                command.Parameters.AddWithValue("@Level", step.Level);
                command.Parameters.AddWithValue("@Name", step.Name);
                command.Parameters.AddWithValue("@Description", step.Description);

                command.Parameters.Add("@StepID", SqlDbType.Int);
                command.Parameters["@StepID"].Direction = ParameterDirection.Output;

                connection.Open();

                command.ExecuteNonQuery();

                step.ID = int.Parse(command.Parameters["@StepID"].Value.ToString());
            }

            return step;
        }

        public void Stop(Step step)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "DL_Process_Step_Stop";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@StepId", step.ID);

                connection.Open();

                command.ExecuteNonQuery();
            }
        }

        public void Log(Log log)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "DL_Process_Step_Log";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@StepID", log.Step.ID);
                command.Parameters.AddWithValue("@StoredProcedure", log.StoredProcedure);
                command.Parameters.AddWithValue("@LineNumber", log.LineNumber);
                command.Parameters.AddWithValue("@LogLevel", log.LogLevel);
                command.Parameters.AddWithValue("@RecordCount", log.RecordCount);
                command.Parameters.AddWithValue("@DataAffected", log.DataAffected);
                command.Parameters.AddWithValue("@Message", log.Message);

                connection.Open();

                command.ExecuteNonQuery();
            }
        }
    }
}