﻿using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace DataFeeds.Logging.Repositories
{
    public class ProcessLoggingRepository
    {
        string _connectionString;

        public ProcessLoggingRepository()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["ProcessLogging"].ConnectionString;
        }

        public Process Start(Process process)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "DL_Process_Start";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@DataSetId", process.DataSetId);
                command.Parameters.AddWithValue("@DataIdentifier", process.DataIdentifier);
                command.Parameters.AddWithValue("@Name", process.Name);
                command.Parameters.AddWithValue("@Description", process.Description);
                command.Parameters.AddWithValue("@Classification", process.Classification);
                command.Parameters.AddWithValue("@DataFeedFrom", process.DataFeedFrom);
                command.Parameters.AddWithValue("@DataFeedTo", process.DataFeedTo);

                command.Parameters.Add("@ProcessID", SqlDbType.Int);
                command.Parameters["@ProcessID"].Direction = ParameterDirection.Output;

                connection.Open();

                command.ExecuteNonQuery();

                process.ID = int.Parse(command.Parameters["@ProcessID"].Value.ToString());
            }

            return process;
        }

        public void Stop(Process process)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "DL_Process_Stop";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@ProcessId", process.ID);

                connection.Open();

                command.ExecuteNonQuery();
            }
        }

        public string GetStatus(Process process)
        {
            string status = string.Empty;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT Status FROM DL_Process WHERE ProcessId = @ProcessId";
                command.CommandType = CommandType.Text;

                command.Parameters.AddWithValue("@ProcessId", process.ID);

                connection.Open();

                return command.ExecuteScalar().ToString();
            }
        }
    }
}