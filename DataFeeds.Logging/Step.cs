﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFeeds.Logging
{
    public class Step
    {
        public int ID { get; set; }
        public Process process { get; set; }
        public string Level { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
