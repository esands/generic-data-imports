﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFeeds.Logging
{
    public class Process
    {
        public int ID { get; set; }
        public int DataSetId { get; set; }
        public string DataIdentifier { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Classification { get; set; }
        public DateTime DataFeedFrom { get; set; }
        public DateTime DataFeedTo { get; set; }
        public string Status { get; set; }
    }
}