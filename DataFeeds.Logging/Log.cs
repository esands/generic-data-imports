﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFeeds.Logging
{
    public class Log
    {
        public int ID { get; set; }
        public Step Step { get; set; }
        public string StoredProcedure { get; set; }
        public int LineNumber { get; set; }
        public string LogLevel { get; set; }
        public int RecordCount { get; set; }
        public string DataAffected { get; set; }
        public string Message { get; set; }
    }
}
