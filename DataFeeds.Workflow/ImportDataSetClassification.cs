﻿using System;
using System.Collections.Generic;
using DataFeeds.Logging;
using DataFeeds.Logging.Repositories;
using DataFeeds.Destination;

namespace DataFeeds.Workflow
{
    public class ImportDataSetClassification
    {
        private DataSetClassificationInfo _dataSetClassificationInfo;
        private Process _process;
        private StepRepository _stepRepository;
        private ProcessingRepository _processingRepository;

        public ImportDataSetClassification(ImportRequirements importRequirements, Process process)
        {
            _dataSetClassificationInfo = new DataSetClassificationInfo(importRequirements);
            _process = process;
            _stepRepository = new StepRepository();
            _processingRepository = new ProcessingRepository(importRequirements.DataSet.System.ConnectionString);
        }

        public void Import()
        {
            Step step = new Step()
            {
                process = _process,
                Level = "I",
                Name = "IMPORT RAW DATA",
                Description = "Imports data from files into raw data _IMPORT tables.  No validation or transformation is performed on the raw data."
            };

            // start a new processsing step
            _stepRepository.Start(step);

            //import all the tables for the data classification
            List<TableInfo> tablesToImport = _dataSetClassificationInfo.TablesToImport;

            foreach (TableInfo tableInfo in tablesToImport)
            {
                ImportTable importTable = new ImportTable(tableInfo, step);
                importTable.Import();
            }

            // end the processing step
            _stepRepository.Stop(step);
        }

        public void Process()
        {
            string processingScript = _dataSetClassificationInfo.ProcessingScript;

            if(!string.IsNullOrEmpty(processingScript))
            {
                try
                {
                    _processingRepository.Process(processingScript, _dataSetClassificationInfo.ImportRequirements.DataSet.ID, _process.ID);
                }
                catch(Exception ex)
                {
                    Step step = new Step()
                    {
                        process = _process,
                        Level = "PL",
                        Name = "PROCESSING",
                        Description = "Processes raw data into presentation format"
                    };

                    // start a new processsing step
                    _stepRepository.Start(step);

                    Log log = new Log();
                    log.Step = step;
                    log.StoredProcedure = "DataFeeds.Workflow.ImportDataSetClassification";
                    log.DataAffected = _dataSetClassificationInfo.ImportRequirements.Classification.Name;
                    log.Message = "Unable to process data with processing script: " + processingScript + ". Error: " + ex.Message;
                    log.LineNumber = 75;
                    log.LogLevel = "ERROR";
                    log.RecordCount = 0;
                    _stepRepository.Log(log);

                    _stepRepository.Stop(step);
                }
            }            
        }
    }
}