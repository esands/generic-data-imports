﻿using System;
using DataFeeds.Configuration;

namespace DataFeeds.Workflow
{
    public class ImportRequirements
    {
        public DataSet DataSet { get; set; }
        public FileInfo ImportFile { get; set; }
        public DataFeeds.Configuration.DataClassification Classification { get; set; }
    }
}