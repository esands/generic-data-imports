﻿using System;
using System.Collections.Generic;
using System.Linq;
using Alchemy.Domain;
using DataFeeds.Configuration;

namespace DataFeeds.Workflow
{
    public class WeeklySchedule : IImportSchedule
    {
        private WeekNumbersRepository _weekNumbersRepository;
        private DataSet _dataSet;

        public WeeklySchedule(DataSet dataSet)
        {
            _weekNumbersRepository = new WeekNumbersRepository();
            _dataSet = dataSet;

            SupplyScheduleType schedule = _dataSet.SupplySchedule.Type;
        }

        public List<FileInfo> GetImportFileInfo(DateTime importStartDate, DateTime importEndDate)
        {
            List<FileInfo> importFiles = new List<FileInfo>();

            // process previous week if date specified is the current date
            if (importStartDate.Date >= DateTime.Now.Date)
            {
                importStartDate = importStartDate.AddDays(-6);
            }

            List<WeekNumber> weekNumbers = _weekNumbersRepository.GetAll(importStartDate, importEndDate);

            // remove files where the week end date is in the future
            weekNumbers.RemoveAll(w => w.EndDate.Date > DateTime.Now.Date);

            foreach (WeekNumber week in weekNumbers)
            {
                FileInfo importFile = new FileInfo();

                importFile.ScheduleStartDate = week.StartDate;
                importFile.ScheduleEndDate = week.EndDate;
                importFile.ScheduleYearNoWeekNo = week.YearNoWeekNo.ToString();

                importFiles.Add(importFile);
            }

            // adjust file dates to match client calendar and file dates

            if (_dataSet.Identifier_Days_Adjust < 0 || _dataSet.Identifier_Days_Adjust > 0)
            {
                int adjustment = _dataSet.Identifier_Days_Adjust;

                DateTime calendarStart = importStartDate.AddDays(adjustment);
                DateTime calendarEnd = importEndDate.AddDays(adjustment);

                List<WeekNumber> calendarWeekNumbers = _weekNumbersRepository.GetAll(calendarStart, calendarEnd);

                foreach (FileInfo fileInfo in importFiles)
                {
                    WeekNumber calendarWeek = calendarWeekNumbers.FirstOrDefault(w => w.StartDate == fileInfo.ScheduleStartDate.AddDays(adjustment));

                    if (calendarWeek != null)
                    {
                        fileInfo.CalendarStartDate = calendarWeek.StartDate;
                        fileInfo.CalendarEndDate = calendarWeek.EndDate;
                        fileInfo.CalendarYearNoWeekNo = calendarWeek.YearNoWeekNo.ToString();
                    }
                }
            }

            return importFiles;
        }
    }
}