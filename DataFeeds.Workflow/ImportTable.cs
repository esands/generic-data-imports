﻿using DataFeeds.Configuration;
using System;
using System.Data;
using Infrastructure.FlatFileReader;
using DataFeeds.Destination;
using DataFeeds.Logging;
using DataFeeds.Logging.Repositories;
using System.Linq;

namespace DataFeeds.Workflow
{
    public class ImportTable
    {
        private TableInfo _table;
        private ImportTableRepository _repository;
        private StepRepository _stepRepository;
        private Step _step;

        public ImportTable(TableInfo table, Step step)
        {
            _table = table;
            _repository = new ImportTableRepository(_table.DataSet.ImportRequirements.DataSet.System.ConnectionString);
            _stepRepository = new StepRepository();
            _step = step;
        }

        public void Import()
        {
            DataTable data = new DataTable();
            ImportResponse importResponse = null;

            Log log = new Log();
            log.Step = _step;
            log.StoredProcedure = "DataFeeds.Workflow.ImportTable";
            log.DataAffected = _table.DataIdentifier;

            try
            {
                data = FlatFileReader.ReadFlatFile(_table.File, _table.DataSet.ImportRequirements.DataSet.HasFieldHeaders, _table.DataSet.ImportRequirements.DataSet.HasFieldQuotes, _table.DataSet.ImportRequirements.DataSet.FieldDelimiter, null);
                DataColumn column = new DataColumn("DataIdentifier");
                column.DefaultValue = _table.DataSet.ImportRequirements.ImportFile.FileDateIdentifier;
                column.DataType = typeof(System.String);
                data.Columns.Add(column);
                column.SetOrdinal(0);
            }
            catch (Exception ex)
            {
                log.Message = "Unable to load data for file: " + _table.File + ". Error: " + ex.Message;
                log.LineNumber = 51;
                log.LogLevel = "ERROR";
                log.RecordCount = 0;
                _stepRepository.Log(log);
                return;
            }
            try
            {
                importResponse = _repository.Import(data, _table.Table);
            }
            catch (Exception ex)
            {
                log.Message = "Unable to import data from file: " + _table.File + " into table: " + _table.Table + ". This can happen when there are additional field delimiters ie commas in the data file. Error: " + ex.Message;
                log.LineNumber = 67;
                log.LogLevel = "ERROR";
                log.RecordCount = 0;
                _stepRepository.Log(log);
                return;
            }

            //success
            log.LineNumber = (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber();
            log.RecordCount = importResponse.recordsImported;
            log.LogLevel = importResponse.recordsImported > 0 ? "INFO" : "WARN";
            log.Message = "Records imported successfully from file " + _table.File + " into table " + _table.Table;

            _stepRepository.Log(log);
        }
    }
}