﻿using System;
using DataFeeds.Configuration;

namespace DataFeeds.Workflow
{
    public static class ScheduleFactory
    {
        //return the required object
        public static IImportSchedule GetScheduleFor(DataSet dataSet)
        {
            IImportSchedule schedule;

            switch (dataSet.SupplySchedule.Type)
            {
                case SupplyScheduleType.DAILY:
                    schedule = new DailySchedule(dataSet);
                    return schedule;
                case SupplyScheduleType.WEEKLY:
                    schedule = new WeeklySchedule(dataSet);
                    return schedule;
                default:
                    throw new ApplicationException(string.Format("schedule not found for supply schedule {0}", dataSet.SupplySchedule.Type.ToString()));
            }
        }
    }
}