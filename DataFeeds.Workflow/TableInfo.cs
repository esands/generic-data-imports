﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataFeeds.Configuration;
using Infrastructure.Utilities;

namespace DataFeeds.Workflow
{
    public class TableInfo
    {
        private DataSetClassificationInfo _dataSet;
        private Table _table;

        public TableInfo (DataSetClassificationInfo dataSet, Table table)
        {
            _dataSet = dataSet;
            _table = table;
        }

        public DataSetClassificationInfo DataSet
        {
            get
            {
                return _dataSet;
            }
        }

        public string File
        {
            get
            {
                return Path.Combine(_dataSet.ImportRequirements.DataSet.FileLocation, FileName);
            }
        }

        public string FileName
        {
            get
            {
                return GetFileName();
            }
        }

        public string Table
        {
            get
            {
                return _dataSet.ImportRequirements.DataSet.TableSet + _table.TableName;
            }
        }

        public string DataIdentifier
        {
            get
            {
                return _table.FileIdentifier;
            }
        }

        private string GetFileName()
        {
            string fileFormat = _table.DataSet.FileFormat;

            fileFormat = ReplaceFileIdentifier(fileFormat);
            fileFormat = ReplaceFileDate(fileFormat);

            fileFormat = fileFormat.Replace("{", "").Replace("}", "");

            return fileFormat;
        }

        private string ReplaceFileIdentifier(string fileName)
        {
            string[] placeholders = _table.DataSet.FileFormat.Split('{', '}');

            string fileIdentifier = placeholders.FirstOrDefault(p => p.ToUpper().Contains("FILEIDENTIFIER"));

            if (fileIdentifier != null)
            {
                fileName = fileName.Replace(fileIdentifier, _table.FileIdentifier);
            }

            return fileName;
        }

        private string ReplaceFileDate(string fileName)
        {
            string[] placeholders = _table.DataSet.FileFormat.Split('{', '}');

            string datePlaceholder = placeholders.FirstOrDefault(p => p.ToUpper().Contains("YY"));

            if (datePlaceholder != null)
            {
                fileName = fileName.Replace(datePlaceholder, _dataSet.ImportRequirements.ImportFile.FileDateIdentifier);
            }

            return fileName;
        }
    }
}