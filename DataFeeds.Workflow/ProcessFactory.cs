﻿using DataFeeds.Logging;

namespace DataFeeds.Workflow
{
    public static class ProcessFactory
    {
        public static Process CreatNewProcessFor(ImportRequirements requirements)
        {
            Process process = new Process();
            process.DataSetId = requirements.DataSet.ID;
            process.DataIdentifier = requirements.ImportFile.FileDateIdentifier;
            process.Name = requirements.DataSet.Name;
            process.Description = requirements.DataSet.Description;
            process.DataFeedFrom = requirements.ImportFile.DataStartDate;
            process.DataFeedTo = requirements.ImportFile.DataEndDate;
            process.Classification = requirements.Classification.Name;

            return process;
        }
    }
}