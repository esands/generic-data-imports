﻿using System;
using System.Collections.Generic;
using DataFeeds.Logging;
using DataFeeds.Logging.Repositories;
using DataFeeds.Destination;
using System.IO;
using System.Linq;
using System.Threading;
using Infrastructure.Compression;

namespace DataFeeds.Workflow
{
    public class DeCompression
    {
        private DataSetClassificationInfo _dataSetClassificationInfo;
        private Process _process;
        private StepRepository _stepRepository;
        private ProcessingRepository _processingRepository;
        private Step _step;

        public DeCompression(ImportRequirements importRequirements, Process process)
        {
            _dataSetClassificationInfo = new DataSetClassificationInfo(importRequirements);
            _process = process;
            _stepRepository = new StepRepository();
        }

        public void DeCompressDataClassification()
        {
            string zipFileIdentifier = string.Empty;
            string zipFileName = string.Empty;

            _step = new Step()
            {
                process = _process,
                Level = "I",
                Name = "DATA EXTRACTION",
                Description = "Extract dataset from zip file."
            };

            // start a new processsing step
            _stepRepository.Start(_step);

            if (FilesRequireExtraction())
            {
                zipFileIdentifier = GetZipFileIdentifier();

                zipFileName = GetZipFileMatchingIdentifier(zipFileIdentifier);

                if (!string.IsNullOrEmpty(zipFileName))
                {
                    ExtractDataClassificationFrom(zipFileName);
                }
            }
            else
            {
                Log log = new Log();
                log.Step = _step;
                log.StoredProcedure = "DataFeeds.Workflow.DeCompression";
                log.DataAffected = _dataSetClassificationInfo.ImportRequirements.Classification.Name;
                log.RecordCount = 0;
                log.Message = "all files in classification already exist - no extraction required";
                log.LineNumber = 63;
                log.LogLevel = "INFO";
                _stepRepository.Log(log);
            }

            // end the processing step
            _stepRepository.Stop(_step);
        }

        private bool FilesRequireExtraction()
        {
            bool response = _dataSetClassificationInfo.TablesToImport.Any(f => !File.Exists(f.File));

            return response;
        }

        private string GetZipFileIdentifier()
        {
            string fileIdentifier = _dataSetClassificationInfo.ImportRequirements.DataSet.Compression.FileFormat;

            fileIdentifier = ReplaceFileDate(fileIdentifier);

            fileIdentifier = fileIdentifier.Substring(0, fileIdentifier.IndexOf("{*}"));

            fileIdentifier = fileIdentifier.Replace("{", "").Replace("}", "");

            //remove the trailing _
            if (fileIdentifier.EndsWith("_"))
            {
                int i = fileIdentifier.LastIndexOf("_");
                fileIdentifier = fileIdentifier.Remove(i,1);
            }
            return fileIdentifier;
        }

        private string ReplaceFileDate(string fileName)
        {
            string[] placeholders = _dataSetClassificationInfo.ImportRequirements.DataSet.Compression.FileFormat.Split('{', '}');

            string datePlaceholder = placeholders.FirstOrDefault(p => p.ToUpper().Contains("YY"));

            if (datePlaceholder != null)
            {
                fileName = fileName.Replace(datePlaceholder, _dataSetClassificationInfo.ImportRequirements.ImportFile.FileDateIdentifier);
            }

            return fileName;
        }

       private string GetZipFileMatchingIdentifier(string zipFileIdentiier)
        {
            FileSystemInfo zipFile;
            string fileName = string.Empty;

            _dataSetClassificationInfo.ImportRequirements.DataSet.FileLocation = "c:/users/matt/test_data/";

            DirectoryInfo dataSetLocation = new DirectoryInfo(_dataSetClassificationInfo.ImportRequirements.DataSet.FileLocation);
            FileSystemInfo[] filesAndDirs = dataSetLocation.GetFileSystemInfos("*" + zipFileIdentiier + "*");
            
            zipFile = filesAndDirs.Where(f => ((System.IO.FileInfo)f).Extension == ".zip").FirstOrDefault();

            if(zipFile != null)
            {
                fileName = zipFile.Name;
            }
            else
            {
                Log log = new Log();
                log.Step = _step;
                log.StoredProcedure = "DataFeeds.Workflow.DeCompression";
                log.DataAffected = _dataSetClassificationInfo.ImportRequirements.Classification.Name;
                log.RecordCount = 0;
                log.Message = string.Format("no zip file found at: {0} with identifier: {1}", _dataSetClassificationInfo.ImportRequirements.DataSet.FileLocation, zipFileIdentiier);
                log.LineNumber = 103;
                log.LogLevel = "ERROR";
                _stepRepository.Log(log);
            }

            return fileName;
        }

        private void ExtractDataClassificationFrom(string zipFileName)
        {
            _dataSetClassificationInfo.ImportRequirements.DataSet.FileLocation = "c:/users/matt/test_data/";
            string zipFile = Path.Combine(_dataSetClassificationInfo.ImportRequirements.DataSet.FileLocation, zipFileName);

            foreach (TableInfo tableInfo in _dataSetClassificationInfo.TablesToImport)
            {
                Log log = new Log();
                log.Step = _step;
                log.StoredProcedure = "DataFeeds.Workflow.DeCompression";
                log.DataAffected = tableInfo.DataIdentifier;
                log.RecordCount = 0;

                if (File.Exists(tableInfo.File))
                {
                    log.Message = string.Format("file: {0} already exists and does not require extraction", tableInfo.File);
                    log.LineNumber = 112;
                    log.LogLevel = "INFO";
                    _stepRepository.Log(log);
                }
                else
                {
                    try
                    {
                        ZipFiles.ExtractFile(_dataSetClassificationInfo.ImportRequirements.DataSet.FileLocation, zipFileName, tableInfo.FileName);
                        log.Message = string.Format("Extracting file: {0} from zip file: {1}", tableInfo.FileName, zipFile);


                        //helpfully, sometimes the files are nested within a folder structure, in these cases we need to move the files up a level
                        //whilst this isnt ideal - its much eaiser then getting the provider to re-process.
                        if (!File.Exists(tableInfo.File))
                        {
                            //extract the file
                            string nestedFileLocation = tableInfo.File.Replace(".csv", "/");
                            string nestedFile =  tableInfo.FileName.Replace(".csv", "/") + tableInfo.FileName;
                            ZipFiles.ExtractFile(_dataSetClassificationInfo.ImportRequirements.DataSet.FileLocation, zipFileName, nestedFile);
                            
                            //move it out of the folder and tidy up
                            File.Move(nestedFileLocation + tableInfo.FileName, tableInfo.File);
                            Directory.Delete(nestedFileLocation);
                            log.Message = string.Format("Extracting file: {0} from zip file: {1} and moved to {2}", tableInfo.FileName, nestedFileLocation, tableInfo.File);

                            //delete...
                        }
        
                        log.LineNumber = 125;
                        log.LogLevel = "INFO";
                        log.RecordCount = 0;
                        _stepRepository.Log(log);

                    }
                    catch(Exception ex)
                    {
                        log.Message = string.Format("Unable to extract file: {0} from zip file {1}. Error: {2}", tableInfo.File, zipFile, ex.Message);
                        log.LineNumber = 133;
                        log.LogLevel = "ERROR";
                        log.RecordCount = 0;
                        _stepRepository.Log(log);
                    }
                }
            }
        }
    }
}