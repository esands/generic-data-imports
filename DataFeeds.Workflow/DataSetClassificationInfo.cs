﻿using System;
using DataFeeds.Configuration;
using System.Collections.Generic;
using System.Linq;

namespace DataFeeds.Workflow
{
    public class DataSetClassificationInfo
    {
        private ImportRequirements _importRequirements;

        public ImportRequirements ImportRequirements
        {
            get
            {
                return _importRequirements;
            }
        }

        public DataSetClassificationInfo(ImportRequirements importRequirements)
        {
            _importRequirements = importRequirements;
        }

        public List<TableInfo> TablesToImport
        {
            get
            {
                List<TableInfo> tablesToImport = new List<TableInfo>();

                tablesToImport = _importRequirements.DataSet.Tables.Where(t => (int)t.DataClassification.Type == (int)_importRequirements.Classification.Type)
                                    .Select(t => new TableInfo(this, t)).ToList();


                return tablesToImport;
            }
        }

        public string ProcessingScript
        {
            get
            {
                
                string processingScript = string.Empty;

                ProcessingScript script = _importRequirements.DataSet.ProcessingScripts.FirstOrDefault(p => (int)p.Classification.Type == (int)_importRequirements.Classification.Type);
                
                if(script != null)
                {
                    processingScript = script.Name;
                }

                return processingScript;
            }
        }
    }
}