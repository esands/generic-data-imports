﻿using System;
using System.Collections.Generic;
using System.Linq;
using Alchemy.Domain;
using DataFeeds.Configuration;

namespace DataFeeds.Workflow
{
    public class DailySchedule : IImportSchedule
    {
        private DataSet _dataSet;

        public DailySchedule(DataSet dataSet)
        {
            _dataSet = dataSet;

            SupplyScheduleType schedule = _dataSet.SupplySchedule.Type;
        }

        public List<FileInfo> GetImportFileInfo(DateTime importStartDate, DateTime importEndDate)
        {
            List<FileInfo> importFiles = new List<FileInfo>();

            string[] placeholders = _dataSet.FileFormat.Split('{', '}');

            string datePlaceholder = placeholders.FirstOrDefault(p => p.ToUpper().Contains("YY")).ToLower().Replace("mm", "MM");

            // cant import files in the future
            if (importEndDate > DateTime.Now.Date)
            {
                importEndDate = DateTime.Now.Date;
            }

            DateTime theDate = importStartDate;
            while (theDate <= importEndDate)
            {
                FileInfo importFile = new FileInfo();

                importFile.ScheduleStartDate = theDate;
                importFile.ScheduleEndDate = theDate;
                importFile.ScheduleYearNoWeekNo = theDate.ToString(datePlaceholder);

                theDate = theDate.AddDays(1);

                importFiles.Add(importFile);
            }
            
            return importFiles;
        }
    }
}