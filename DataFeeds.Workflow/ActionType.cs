﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFeeds.Workflow
{
    public enum ActionType
    {
        IMPORTANDPROCESS,
        PROCESS,
        IMPORT
    }
}