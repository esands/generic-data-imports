﻿using System;
using DataFeeds.Logging;

namespace DataFeeds.Workflow
{
    public delegate void ProcessingDelegate(Process process, string fileDateIdentifier);

    public class ImportDataSetRequest
    {
        private DateTime _to;

        public ProcessingDelegate ProcessingCallBack { get; set; }

        public int DataSetId { get; set; }

        public DataClassification Classification { get; set; }

        public DateTime From { get; set; }

        public DateTime To
        {
            set
            {
                _to = value;
            }
            get
            {
                return _to == DateTime.MinValue ? From : _to;
            }
        }

        public ActionType Action { get; set; }
    }
}