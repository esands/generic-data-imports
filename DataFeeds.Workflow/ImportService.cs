﻿
using DataFeeds.Configuration;
using Alchemy.Domain;
using System.Collections.Generic;
using System.Linq;
using DataFeeds.Logging;
using DataFeeds.Logging.Repositories;

namespace DataFeeds.Workflow
{
    public class ImportService
    {
        private DataSetRepository _dataSetRepository;
        private ProcessLoggingRepository _processLoggingRepository;

        public ImportService()
        {
            _dataSetRepository = new DataSetRepository();
            _processLoggingRepository = new ProcessLoggingRepository();
        }

        public void ImportDataSet(ImportDataSetRequest request)
        {
            DataSet dataSetToImport = _dataSetRepository.Get(request.DataSetId);

            IImportSchedule schedule = ScheduleFactory.GetScheduleFor(dataSetToImport);

            List<FileInfo> filesToImport = schedule.GetImportFileInfo(request.From, request.To);

            List<DataFeeds.Configuration.DataClassification> classificationsToImport = dataSetToImport.ClassificationsInDataSet
                            .Where(c => (int)c.Type == (int)request.Classification || request.Classification == DataClassification.ALL).OrderBy(k => k.ID).ToList();

            foreach (DataFeeds.Configuration.DataClassification classification in classificationsToImport)
            {
                foreach (FileInfo importFile in filesToImport)
                {
                    ImportRequirements importRequirements = new ImportRequirements()
                    {
                        DataSet = dataSetToImport,
                        ImportFile = importFile,
                        Classification = classification
                    };

                    Process process = ProcessFactory.CreatNewProcessFor(importRequirements);
                    _processLoggingRepository.Start(process);

                    if(dataSetToImport.IsCompressed)
                    {
                        DeCompression deCompression = new DeCompression(importRequirements, process);
                        deCompression.DeCompressDataClassification();
                    }

                    ImportDataSetClassification importer = new ImportDataSetClassification(importRequirements, process);

                    if (request.Action != ActionType.PROCESS)   // no need to import if we know the data to process is already in import tables
                    {
                        importer.Import();
                    }

                    if (request.Action != ActionType.IMPORT)    // no need to process if we want to import data for review
                    {
                        importer.Process();
                    }

                    _processLoggingRepository.Stop(process);

                    process.Status = _processLoggingRepository.GetStatus(process);

                    request.ProcessingCallBack(process, importFile.FileDateIdentifier);
                }
            }
        }
    }
}