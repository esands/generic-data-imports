﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFeeds.Workflow
{
    public enum DataClassification
    {
        MASTER = 1,
        SALESOUT = 2,
        SALESIN = 3,
        ALL
    }
}