﻿using System;
using Alchemy.Domain;

namespace DataFeeds.Workflow
{
    public class FileInfo
    {
        public DateTime ScheduleStartDate { get; set; }
        public DateTime ScheduleEndDate { get; set; }
        public string ScheduleYearNoWeekNo { get; set; }
        public DateTime CalendarStartDate { get; set; }
        public DateTime CalendarEndDate { get; set; }
        public string CalendarYearNoWeekNo { get; set; }

        public string FileDateIdentifier
        {
            get
            {
                return string.IsNullOrEmpty(CalendarYearNoWeekNo)  ? ScheduleYearNoWeekNo : CalendarYearNoWeekNo;
            }
        }

        public DateTime DataStartDate
        {
            get
            {
                return ScheduleStartDate;
            }
        }

        public DateTime DataEndDate
        {
            get
            {
                return ScheduleEndDate;
            }
        }
    }
}