﻿using System;
using System.Collections.Generic;
using System.Linq;
using Alchemy.Domain;
using DataFeeds.Configuration;

namespace DataFeeds.Workflow
{
    public interface IImportSchedule
    {
        List<FileInfo> GetImportFileInfo(DateTime importStartDate, DateTime importEndDate);
    }
}