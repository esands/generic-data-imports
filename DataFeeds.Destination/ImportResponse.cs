﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFeeds.Destination
{
   public class ImportResponse
    {
        public string TableName { get; set; }
        public int recordsImported { get; set; }
    }
}
