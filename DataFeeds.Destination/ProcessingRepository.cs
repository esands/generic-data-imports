﻿using System.Data;
using System.Data.SqlClient;

namespace DataFeeds.Destination
{
    public class ProcessingRepository
    {
        protected string _connectionString;

        public ProcessingRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void Process(string processingScript, int dataSetId, int processLogId)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandTimeout = 10000;
                command.CommandText = processingScript;
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@DataSetId", dataSetId);
                command.Parameters.AddWithValue("@ProcessLogId", processLogId);

                connection.Open();

                command.ExecuteNonQuery();
            }
        }
    }
}