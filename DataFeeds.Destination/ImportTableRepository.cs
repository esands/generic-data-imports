﻿using System.Data;
using System.Data.SqlClient;

namespace DataFeeds.Destination
{
    public class ImportTableRepository
    {
        protected string _connectionString;

        public ImportTableRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public ImportResponse Import(DataTable tableToimport, string tableName)
        {
            ImportResponse response = new ImportResponse();

            // truncate import table
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandTimeout = 5000;
                command.CommandText = "TRUNCATE TABLE " + tableName;
                command.CommandType = CommandType.Text;

                connection.Open();

                command.ExecuteNonQuery();
            }

            //import
            using (var sqlBulk = new SqlBulkCopy(_connectionString))
            {
                sqlBulk.DestinationTableName = tableName;
                sqlBulk.BatchSize = 10000;
                sqlBulk.BulkCopyTimeout = 5000;
                sqlBulk.WriteToServer(tableToimport);

                string s = "s";
            }

            // get record count
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandTimeout = 5000;
                command.CommandText = "SELECT COUNT(*) FROM " + tableName;
                command.CommandType = CommandType.Text;

                connection.Open();

                response.recordsImported = int.Parse(command.ExecuteScalar().ToString());
            }

            return response;
        }
    }
}