﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Alchemy.Domain
{
    public class WeekNumbersRepository
    {
        string _connectionString;

        public WeekNumbersRepository()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["Alchemy"].ConnectionString;
        }

        public List<WeekNumber> GetAll(DateTime From, DateTime To)
        {
            List<WeekNumber> weekNumbers = new List<WeekNumber>();
            WeekNumber weekNumber = null;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM Week_Numbers WHERE" +
                                            " @StartDate BETWEEN StartDate AND EndDate" +
                                            " OR " +
                                            " StartDate >= @StartDate AND EndDate <= @EndDate" +
                                            " OR " +
                                            " @EndDate BETWEEN StartDate AND EndDate" +
                                            " ORDER BY StartDate";
                command.CommandType = CommandType.Text;

                command.Parameters.AddWithValue("@StartDate", From);
                command.Parameters.AddWithValue("@EndDate", To);

                connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows == true)
                    {
                        while (reader.Read())
                        {
                            weekNumber = CreateWeekNumberFrom(reader);
                            weekNumbers.Add(weekNumber);
                        }
                    }
                }
            }

            return weekNumbers;
        }

        private WeekNumber CreateWeekNumberFrom(IDataReader dataReader)
        {
            WeekNumber weekNumber = new WeekNumber();

            weekNumber.WeekNo = dataReader.GetInt32(dataReader.GetOrdinal("WeekNo"));
            weekNumber.YearNo = dataReader.GetInt32(dataReader.GetOrdinal("YearNo"));
            weekNumber.YearNoWeekNo = dataReader.GetInt32(dataReader.GetOrdinal("YearNoWeekNo"));
            weekNumber.StartDate = dataReader.GetDateTime(dataReader.GetOrdinal("StartDate"));
            weekNumber.EndDate = dataReader.GetDateTime(dataReader.GetOrdinal("EndDate"));

            return weekNumber;
        }
    }
}
