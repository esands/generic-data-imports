﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alchemy.Domain
{
    public class WeekNumber
    {
        public int WeekNo { get; set; }
        public int YearNo { get; set; }
        public int YearNoWeekNo { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}