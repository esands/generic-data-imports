﻿using System;
using CommandLine;
using DataFeeds.Workflow;

namespace CRG.DataImports.Wholesaler.Admin
{
    public class ProgramArguments
    {
        //set default values for all parameters
        private DateTime _dataFeedFrom = DateTime.Today;
        private DateTime _dataFeedTo = DateTime.Today;
        private DataClassification _classification = DataClassification.ALL;
        private ActionType _action = ActionType.IMPORTANDPROCESS;

        //Required
        [Option('z', "DatasetId", Required = false)]
        public int DataSetId { get; set; }

        [Option('c', "Classification", Required = false)]
        public string Classification
        {
            get
            {
                return _classification.ToString();
            }
            set
            {
                string classification = value;
                DataClassification parsedClassification;
                if (Enum.TryParse(classification, out parsedClassification))
                {
                    _classification = (DataClassification)Enum.Parse(typeof(DataClassification), value);
                }
            }
        }

        [Option('a', "Action", Required = false)]
        public string Action
        {
            get
            {
                return _action.ToString();
            }
            set
            {
                string action = value;
                ActionType parsedAction;
                if (Enum.TryParse(action, out parsedAction))
                {
                    _action = (ActionType)Enum.Parse(typeof(ActionType), action);
                }
            }
        }

        [Option('s', "DataFeedFrom", Required = false)]
        public string DataFeedFrom
        {
            get
            {
                return _dataFeedFrom.ToString("d");
            }
            set
            {
                string date = value;
                DateTime parsedDate;
                if (DateTime.TryParse(date, out parsedDate))
                {
                    _dataFeedFrom = parsedDate;
                }
                else
                {
                    _dataFeedFrom = DateTime.Today;     //default start date to current day
                }
            }
        }

        [Option('f', "DataFeedTo", Required = false)]
        public string DataFeedTo
        {
            get
            {
                return _dataFeedTo.ToString("d");
            }
            set
            {
                string date = value;
                DateTime parsedDate;
                if (DateTime.TryParse(date, out parsedDate))
                {
                    _dataFeedTo = parsedDate;
                }
                else
                {
                    _dataFeedTo = DateTime.Today;     //default finish date to current day
                }
            }
        }

        [Option('h', "help", Required = false)]
        public string Help
        {
            get
            {
                string help;

                help = "DataFeeds.Admin.exe is an Admin tool to import and process a dataset within the date range specified.  \n\n" +
                        "This can be used for admin tasks and gap fill to resolve data issues.  \n\n" +
                        "Example Use: DataFeeds.Admin -z 1 -c MASTER -a IMPORT -s 18/09/18 -f 18/09/18 \n\n" +
                        "Parameter list:\n" +
                        "\t-z:\tNumeric ID of the dataset to import (id in datasets table).\n" +
                        "\t-c:\tData classification (MASTER / SALESIN / SALESOUT / ALL).\n" +
                        "\t-a:\tAction (IMPORTANDPROCESS / IMPORT / PROCESS).\n" +
                        "\t-s:\tStart date in format dd/mm/yy (eg 18/09/18).\n" +
                        "\t-f:\tEnd date in format dd/mm/yy (eg 18/09/18).\n";
                return help;
            }
        }

        public DataClassification ClassificationParsed { get { return _classification; } }
        public ActionType ActionParsed { get { return _action; } }
        public DateTime From { get { return _dataFeedFrom; } }
        public DateTime To { get { return _dataFeedTo; } }

        public bool IsValid()
        {
            bool isValid = true;

            if (DataSetId == 0)
            {
                isValid = false;
            }

            return isValid;
        }
    }
}