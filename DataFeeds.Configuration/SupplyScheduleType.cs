﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFeeds.Configuration
{
    public enum SupplyScheduleType
    {
        DAILY = 1,
        WEEKLY = 2
    }
}