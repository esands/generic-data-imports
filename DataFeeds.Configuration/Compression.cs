﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFeeds.Configuration
{
    public class Compression
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string FileFormat { get; set; }
    }
}