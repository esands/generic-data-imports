﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFeeds.Configuration
{
    public class System
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}