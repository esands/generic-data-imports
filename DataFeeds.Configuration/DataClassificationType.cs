﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFeeds.Configuration
{
    public enum DataClassificationType
    {
        MASTER = 1,
        SALESOUT = 2,
        SALESIN = 3
    }
}