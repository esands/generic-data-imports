﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFeeds.Configuration
{
    public class ProcessingScript
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public DataClassification Classification { get; set; }

    }
}