﻿using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;

namespace DataFeeds.Configuration
{
    public class DataSetRepository
    {
        string _connectionString;

        public DataSetRepository()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["DataFeeds"].ConnectionString;
        }

        public DataSet Get(int DataSetId)
        {
            DataSet dataset = null;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM DataSets WHERE DataSetId = @Id";
                command.CommandType = CommandType.Text;

                command.Parameters.AddWithValue("@Id", DataSetId);

                connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows == true)
                    {
                        dataset = CreateDataSetFrom(reader);
                    }
                }
            }

            return dataset;
        }

        private DataSet CreateDataSetFrom(IDataReader dataReader)
        {
            DataSet dataset = null;

            while (dataReader.Read())
            {
                dataset = new DataSet();
                dataset.ID = dataReader.GetInt32(dataReader.GetOrdinal("DataSetId"));
                dataset.Name = dataReader.GetString(dataReader.GetOrdinal("Name"));
                dataset.TableSet = dataReader.GetString(dataReader.GetOrdinal("TableSet"));
                dataset.Description = dataReader.GetString(dataReader.GetOrdinal("Description"));
                dataset.FileLocation = dataReader.GetString(dataReader.GetOrdinal("FileLocation"));
                dataset.FileFormat = dataReader.GetString(dataReader.GetOrdinal("FileFormat"));
                dataset.HasFieldHeaders = dataReader.GetBoolean(dataReader.GetOrdinal("HasFieldHeaders"));
                dataset.HasFieldQuotes = dataReader.GetBoolean(dataReader.GetOrdinal("HasFieldQuotes"));
                dataset.FieldDelimiter = dataReader.GetString(dataReader.GetOrdinal("FieldDelimiter"));
                dataset.Identifier_Days_Adjust = dataReader.GetInt32(dataReader.GetOrdinal("Identifier_Days_Adjust"));
                dataset.Tables = GetTables(dataset);
                dataset.Compression = GetCompression(dataset.ID);

                int systemId = dataReader.GetInt32(dataReader.GetOrdinal("System"));
                dataset.System = GetSystem(systemId);

                int dataSourceId = dataReader.GetInt32(dataReader.GetOrdinal("DataSource"));
                dataset.DataSource = GetDataSource(dataSourceId);

                int supplyScheduleId = dataReader.GetInt32(dataReader.GetOrdinal("SupplySchedule"));
                dataset.SupplySchedule = GetSupplySchedule(supplyScheduleId);

                dataset.ProcessingScripts = GetProcessingScipts(dataset.ID);
            }

            return dataset;
        }

        private System GetSystem(int systemId)
        {
            System system = null;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM System WHERE SystemId = @Id";
                command.CommandType = CommandType.Text;

                command.Parameters.AddWithValue("@Id", systemId);

                connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows == true)
                    {
                        while (reader.Read())
                        {
                            system = CreateSystemFrom(reader);
                        }
                    }
                }
            }

            return system;
        }

        private System CreateSystemFrom(IDataReader dataReader)
        {
            System system = null;

            system = new System();
            system.ID = dataReader.GetInt32(dataReader.GetOrdinal("SystemId"));
            system.Name = dataReader.GetString(dataReader.GetOrdinal("Name"));
            system.Description = dataReader.GetString(dataReader.GetOrdinal("Description"));
            system.Database = dataReader.GetString(dataReader.GetOrdinal("Database"));
            system.ConnectionString = dataReader.GetString(dataReader.GetOrdinal("ConnectionString"));

            return system;
        }

        private List<Table> GetTables(DataSet dataSet)
        {
            List<Table> tables = new List<Table>();
            Table table = null;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM Tables WHERE DataSet = @Id";
                command.CommandType = CommandType.Text;

                command.Parameters.AddWithValue("@Id", dataSet.ID);

                connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows == true)
                    {
                        while (reader.Read())
                        {
                            table = CreateTableFrom(reader);
                            table.DataSet = dataSet;
                            tables.Add(table);
                        }
                    }
                }
            }

            return tables;
        }

        private Table CreateTableFrom(IDataReader dataReader)
        {
            Table table = null;

            table = new Table();
            table.ID = dataReader.GetInt32(dataReader.GetOrdinal("TableId"));
            table.TableName = dataReader.GetString(dataReader.GetOrdinal("Table"));
            table.FileIdentifier = dataReader.GetString(dataReader.GetOrdinal("FileIdentifier"));

            int dataClassificationId = dataReader.GetInt32(dataReader.GetOrdinal("Classification"));
            table.DataClassification = GetDataClassification(dataClassificationId);

            return table;
        }

        private DataSource GetDataSource(int dataSourceId)
        {
            DataSource dataSource = null;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM DataSources WHERE DataSourceId = @Id";
                command.CommandType = CommandType.Text;

                command.Parameters.AddWithValue("@Id", dataSourceId);

                connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows == true)
                    {
                        while (reader.Read())
                        {
                            dataSource = CreateDataSourceFrom(reader);
                        }
                    }
                }
            }

            return dataSource;
        }

        private DataSource CreateDataSourceFrom(IDataReader dataReader)
        {
            DataSource dataSource = null;

            dataSource = new DataSource();
            dataSource.ID = dataReader.GetInt32(dataReader.GetOrdinal("DataSourceId"));
            dataSource.Name = dataReader.GetString(dataReader.GetOrdinal("Name"));
            dataSource.Description = dataReader.GetString(dataReader.GetOrdinal("Description"));

            return dataSource;
        }

        private DataClassification GetDataClassification(int dataClassificationId)
        {
            DataClassification dataClassification = null;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM DataClassifications WHERE DataClassificationId = @Id";
                command.CommandType = CommandType.Text;

                command.Parameters.AddWithValue("@Id", dataClassificationId);

                connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows == true)
                    {
                        while (reader.Read())
                        {
                            dataClassification = CreateDataClassificationFrom(reader);
                        }
                    }
                }
            }

            return dataClassification;
        }

        private DataClassification CreateDataClassificationFrom(IDataReader dataReader)
        {
            DataClassification dataClassification = null;

            dataClassification = new DataClassification();
            dataClassification.ID = dataReader.GetInt32(dataReader.GetOrdinal("DataClassificationId"));
            dataClassification.Identifier = dataReader.GetString(dataReader.GetOrdinal("Identifier"));
            dataClassification.Name = dataReader.GetString(dataReader.GetOrdinal("Name"));
            dataClassification.Description = dataReader.GetString(dataReader.GetOrdinal("Description"));

            return dataClassification;
        }

        private List<ProcessingScript> GetProcessingScipts(int dataSetId)
        {
            List<ProcessingScript> processingScripts = new List<ProcessingScript>();
            ProcessingScript processingScript = null;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM ProcessingScripts WHERE DataSet = @Id";
                command.CommandType = CommandType.Text;

                command.Parameters.AddWithValue("@Id", dataSetId);

                connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows == true)
                    {
                        while (reader.Read())
                        {
                            processingScript = CreateProcessingScriptFrom(reader);
                            processingScripts.Add(processingScript);
                        }
                    }
                }
            }

            return processingScripts;
        }

        private ProcessingScript CreateProcessingScriptFrom(IDataReader dataReader)
        {
            ProcessingScript processingScript = null;

            processingScript = new ProcessingScript();
            processingScript.ID = dataReader.GetInt32(dataReader.GetOrdinal("ProcessingScriptId"));
            processingScript.Name = dataReader.GetString(dataReader.GetOrdinal("Name"));

            int dataClassificationId = dataReader.GetInt32(dataReader.GetOrdinal("Classification"));
            processingScript.Classification = GetDataClassification(dataClassificationId);

            return processingScript;
        }

        private SupplySchedule GetSupplySchedule(int supplyScheduleId)
        {
            SupplySchedule supplySchedule = null;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM SupplySchedule WHERE supplyScheduleId = @Id";
                command.CommandType = CommandType.Text;

                command.Parameters.AddWithValue("@Id", supplyScheduleId);

                connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows == true)
                    {
                        while (reader.Read())
                        {
                            supplySchedule = CreateSupplyScheduleFrom(reader);
 
                        }
                    }
                }
            }

            return supplySchedule;
        }

        private SupplySchedule CreateSupplyScheduleFrom(IDataReader dataReader)
        {
            SupplySchedule supplySchedule = null;

            supplySchedule = new SupplySchedule();
            supplySchedule.ID = dataReader.GetInt32(dataReader.GetOrdinal("SupplyScheduleId"));
            supplySchedule.Name = dataReader.GetString(dataReader.GetOrdinal("Name"));
            supplySchedule.Description = dataReader.GetString(dataReader.GetOrdinal("Description"));

            return supplySchedule;
        }

        private Compression GetCompression(int dataSetId)
        {
            Compression compression = null;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM Compression WHERE DataSet = @Id";
                command.CommandType = CommandType.Text;

                command.Parameters.AddWithValue("@Id", dataSetId);

                connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows == true)
                    {
                        while (reader.Read())
                        {
                            compression = new Compression();
                            compression = CreateCompressionFrom(reader);
                        }
                    }
                }
            }

            return compression;
        }

        private Compression CreateCompressionFrom(IDataReader dataReader)
        {
            Compression compression = new Compression();

            compression.ID = dataReader.GetInt32(dataReader.GetOrdinal("CompressionId"));
            compression.Name = dataReader.GetString(dataReader.GetOrdinal("Name"));
            compression.Description = dataReader.GetString(dataReader.GetOrdinal("Description"));
            compression.FileFormat = dataReader.GetString(dataReader.GetOrdinal("FileFormat"));

            return compression;
        }
    }
}