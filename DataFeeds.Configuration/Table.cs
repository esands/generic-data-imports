﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFeeds.Configuration
{
    public class Table
    {
        public int ID { get; set; }
        public DataSet DataSet { get; set; }
        public DataClassification DataClassification { get; set; }
        public string TableName { get; set; }
        public string FileIdentifier { get; set; }
    }
}