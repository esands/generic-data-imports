﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFeeds.Configuration
{
    public class SupplySchedule
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public SupplyScheduleType Type
        {
            get
            {
                return (SupplyScheduleType)ID;
            }
        }
    }
}