﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFeeds.Configuration
{
    public class DataSet
    {
        public int ID { get; set; }
        public System System { get; set; }
        public DataSource DataSource { get; set; }
        public List<Table> Tables { get; set; }
        public List<ProcessingScript> ProcessingScripts { get; set; }
        public SupplySchedule SupplySchedule { get; set; }
        public string TableSet { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string FileLocation { get; set; }
        public string FileFormat { get; set; }
        public bool HasFieldHeaders { get; set; }
        public bool HasFieldQuotes { get; set; }
        public string FieldDelimiter { get; set; }
        public Compression Compression { get; set; }
        public int Identifier_Days_Adjust { get; set; }

        public List<DataClassification> ClassificationsInDataSet
        {
            get
            {
                List<DataClassification> classifications = new List<DataClassification>();

                classifications = Tables.GroupBy(t => t.DataClassification.ID).Select(t => t.FirstOrDefault().DataClassification).ToList();

                return classifications;
            }
        }

        public bool IsCompressed
        {
            get
            {
                return Compression != null;
            }
        }
    }
}