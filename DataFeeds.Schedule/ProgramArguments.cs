﻿using System;
using CommandLine;
using DataFeeds.Workflow;

namespace DataFeeds.Schedule
{
    public class ProgramArguments
    {
        private Schedule _dateTypes = Schedule.CURRENT;
        private DateTime _importDate = DateTime.Today;
        private DataClassification _classification = DataClassification.ALL;
        private ActionType _action = ActionType.IMPORTANDPROCESS;

        [Option('z', "DatasetId", Required=false)]
        public int DataSetId { get; set; }

        [Option('c', "Classification", Required = false)]
        public string Classification
        {
            get
            {
                    return _classification.ToString();
            }
            set
            {
                string classification = value;
                DataClassification parsedClassification;
                if (Enum.TryParse(classification, out parsedClassification))
                {
                    _classification = (DataClassification)Enum.Parse(typeof(DataClassification), value);
                }
            }
        }

        [Option('a', "Action", Required = false)]
        public string Action
        {
            get
            {
                return _action.ToString();
            }
            set
            {
                string action = value;
                ActionType parsedAction;
                if (Enum.TryParse(action, out parsedAction))
                {
                    _action = (ActionType)Enum.Parse(typeof(ActionType), action);
                }
            }
        }

        [Option('d', "DateToImport", Required = false)]
        public string DateToImport
        {
            get
            {

                if (_dateTypes == Schedule.UNKNOWN)
                    return string.Empty;
                else
                    return _dateTypes.ToString();
            }
            set
            {
                string dateToImport = value;
                Schedule parsedDateToImport;
                if (Enum.TryParse(dateToImport, out parsedDateToImport))
                {
                    _dateTypes = (Schedule)Enum.Parse(typeof(Schedule), dateToImport);
                }
            }
        }

        [Option('h', "help", Required = false)]
        public string Help
        {
            get
            {
                string help;

                help = "CRG.DataImports.Wholesaler.Schedule.exe is designed to be a scheduled routine to import wholesaler data.\n\n" +
                        "Example Use: CRG.DataImports.Wholesaler.Schedule -z 1 -d TEST -c MASTER\n\n" +
                        "Parameter list:\n" +
                        "\t-f:\tFunction (IMPORT / ADMIN).\n" +
                        "\t-z:\tNumeric ID of the dataset to import (id in datasets table).\n" +
                        "\t-c:\tData classification (MASTER / SALESIN / SALESOUT / ALL).\n" +
                        "\t-a:\tAction (IMPORTANDPROCESS / IMPORT / PROCESS).\n" +
                        "\t-d:\tThe date to import (CURRENT / YESTERDAY / TEST).\n";
                return help;
            }
        }

        public DataClassification ClassificationParsed { get { return _classification; } }
        public ActionType ActionParsed { get { return _action; } }

        public DateTime ImportDate
        {
            get
            {
                DateTime importdate;

                if (_dateTypes == Schedule.TEST)
                    importdate = new DateTime(2019, 02, 27);     // test files saved with dates 18/09/18
                else if (_dateTypes == Schedule.YESTERDAY)
                    importdate = DateTime.Today.AddDays(-1);     // files may be sent with the date of the previous day
                else if (_dateTypes == Schedule.WEEKLY)
                    importdate = DateTime.Today.AddDays(-6);     // files with date of previous week
                else
                    importdate = DateTime.Today;                   // otherwise default to current day

                return importdate;
            }
        }

        public bool IsValid()
        {
            bool isValid = true;

            if (DataSetId == 0)
            {
                isValid = false;
            }

            return isValid;
        }
    }
}