﻿using System;
using CommandLine;
using DataFeeds.Workflow;
using DataFeeds.Logging;

namespace DataFeeds.Schedule
{
    class Program
    {
        static void Main(string[] args)
        {
            ProgramArguments programAguments = new ProgramArguments();

            bool argumentsValid = Parser.Default.ParseArguments(args, programAguments);

            if (!argumentsValid || !programAguments.IsValid()) //additional custom validation required on date due to date parsing issues
            {
                Console.Write(programAguments.Help);
            }
            else if (argumentsValid && programAguments.IsValid())   // lets do something
            {
                ImportService importService = new ImportService();

                ImportDataSetRequest request = new ImportDataSetRequest()
                {
                    ProcessingCallBack = WriteResponseToConsole,
                    DataSetId = programAguments.DataSetId,
                    From = programAguments.ImportDate,
                    Classification = programAguments.ClassificationParsed,
                    Action = programAguments.ActionParsed
                };

                importService.ImportDataSet(request);
            }
        }

        static void WriteResponseToConsole(Process process, string fileDateIdentifier)
        {
            Console.WriteLine("Process ID: " + process.ID + " - Classification: " + process.Classification.ToString() + ", FileIdentifier: " + fileDateIdentifier + ", From: " + process.DataFeedFrom.ToString("d") + ", To: " + process.DataFeedTo.ToString("d") + ", Status: " + process.Status);
        }
    }
}